class CardGame {
  constructor() {
    // Inicializace proměnných pro hru
    this.deck = []; // Balíček karet
    this.players = []; // Seznam hráčů
    this.currentPlayerIndex = 0; // Index aktuálního hráče
    this.dealerHand = []; // Ruka krupiéra
    this.result = document.getElementById('result'); // Element pro zobrazení výsledku
    this.startBtn = document.getElementById('start-btn'); // Tlačítko pro spuštění hry
    this.hitBtn = document.getElementById('hit-btn'); // Tlačítko pro tah hráče
    this.standBtn = document.getElementById('stand-btn'); // Tlačítko pro zůstání u současné ruky hráče
    this.playerHandsElem = document.getElementById('player-hands'); // Element pro zobrazení ruk hráčů
    this.dealerHandElem = document.getElementById('dealer-hand'); // Element pro zobrazení ruky krupiéra
    this.playerCountElem = document.getElementById('player-count'); // Element pro zadání počtu hráčů
    // Přiřazení událostí k tlačítkům
    this.startBtn.addEventListener('click', this.startGame.bind(this)); // Spuštění hry
    this.hitBtn.addEventListener('click', this.hit.bind(this)); // Tah hráče
    this.standBtn.addEventListener('click', this.stand.bind(this)); // Zůstání u současné ruky
  }

  // Metoda pro spuštění hry
  startGame() {
    // Načtení počtu hráčů z formuláře
    const playerCount = parseInt(this.playerCountElem.value);
    // Vytvoření nového balíčku karet
    this.deck = this.createDeck();
    // Inicializace hráčů se dvěma kartami každý
    this.players = Array.from({ length: playerCount }, () => ({
      hand: [this.drawCard(), this.drawCard()],
    }));
    // Krupiérova ruka se dvěma kartami
    this.dealerHand = [this.drawCard(), this.drawCard()];
    // Nastavení aktuálního hráče na prvního v seznamu
    this.currentPlayerIndex = 0;
    // Zobrazení ruk hráčů a krupiéra
    this.displayHands();
    // Kontrola, zda některý z hráčů nedosáhl blackjacku
    this.checkBlackjack();
    // Povolení tlačítek hit a stand, zakázání tlačítka pro spuštění hry
    this.toggleButtons(true);
    // Vyčištění zobrazení výsledku z předchozí hry
    this.result.innerText = '';
  }

  // Metoda pro vytvoření nového balíčku karet
  createDeck() {
    let deck = [];
    const suits = ['♥ ', '♦', '♣', '♠'];
    const values = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
    for (let suit of suits) {
      for (let value of values) {
        deck.push({ value: value, suit: suit });
      }
    }
    return deck;
  }

  // Metoda pro vylosování náhodné karty z balíčku
  drawCard() {
    const index = Math.floor(Math.random() * this.deck.length);
    return this.deck.splice(index, 1)[0];
  }

  // Metoda pro zobrazení ruk hráčů a krupiéra
  displayHands(showDealerCards = false) {
    // Vyčištění zobrazení ruk hráčů a krupiéra
    this.playerHandsElem.innerHTML = '';
    this.dealerHandElem.innerHTML = '';
    // Zobrazení ruk hráčů
    this.players.forEach((player, index) => {
      const playerHandElem = document.createElement('div');
      playerHandElem.classList.add('hand', 'player-hand');
      playerHandElem.innerHTML = `<h2>Player ${index + 1}</h2>`;
      const cardsRow = document.createElement('div');
      cardsRow.classList.add('cards-row');
      this.displayHand(player.hand, cardsRow);
      playerHandElem.appendChild(cardsRow);
      this.playerHandsElem.appendChild(playerHandElem);
    });
    // Zobrazení ruky krupiéra
    const dealerCardsRow = document.createElement('div');
    dealerCardsRow.classList.add('cards-row');
    this.displayHand(this.dealerHand, dealerCardsRow, !showDealerCards);
    this.dealerHandElem.appendChild(dealerCardsRow);
  }

  // Metoda pro zobrazení karet v ruce
  displayHand(hand, targetElem, hideFirstCard = false) {
    for (let i = 0; i < hand.length; i++) {
      const card = hand[i];
      const cardElem = document.createElement('div');
      cardElem.classList.add('card');
      if (hideFirstCard && i === 0) {
        cardElem.textContent = '🂠'; // Unicode pro zadní stranu karty
      } else {
        cardElem.textContent = `${card.value} ${card.suit}`;
      }
      targetElem.appendChild(cardElem);
    }
  }

  // Metoda pro kontrolu blackjacku
  checkBlackjack() {
    this.players.forEach((player, index) => {
      if (this.getHandValue(player.hand) === 21) {
        this.result.innerText += `Hráč ${index + 1} má Blackjack! `;
        this.toggleButtons(false);
        this.displayHands(true); // Odkrytí krupiérovy ruky
      }
    });
  }

  // Metoda pro získání hodnoty ruky
  getHandValue(hand) {
    let value = 0;
    let aceCount = 0;
    for (let card of hand) {
      if (card.value === 'A') {
        aceCount++;
        value += 11;
      } else if (card.value === 'J' || card.value === 'Q' || card.value === 'K') {
        value += 10;
      } else {
        value += parseInt(card.value);
      }
    }
    while (value > 21 && aceCount > 0) {
      value -= 10;
      aceCount--;
    }
    return value;
  }


   // Metoda pro tahu hráče
   hit() {
    // Hráč tahuje kartu
    const player = this.players[this.currentPlayerIndex];
    player.hand.push(this.drawCard());
    // Zobrazení aktualizovaných ruk hráčů
    this.displayHands();
    // Pokud hráč překročí 21 bodů, označí se jako "busted" a přechází se na dalšího hráče
    if (this.getHandValue(player.hand) > 21) {
      this.result.innerText += `Hráč ${this.currentPlayerIndex + 1} přes! `;
      this.nextPlayer();
    }
  }

  // Metoda pro zůstání u současné ruky hráče
  stand() {
    this.nextPlayer();
  }

  // Metoda pro přechod na dalšího hráče nebo tah krupiéra
  nextPlayer() {
    this.currentPlayerIndex++;
    // Pokud byl proveden tah všech hráčů, přechází se na tah krupiéra
    if (this.currentPlayerIndex >= this.players.length) {
      this.dealerTurn();
    }
  }

  // Metoda pro tah krupiéra
  dealerTurn() {
    // Krupiér tahá karty, dokud má méně než 17 bodů v ruce
    while (this.getHandValue(this.dealerHand) < 17) {
      this.dealerHand.push(this.drawCard());
    }
    // Zobrazení ruky krupiéra
    this.displayHands(true);
    // Kontrola vítěze po ukončení tahů krupiéra
    this.checkWinner();
  }

  // Metoda pro kontrolu vítěze
  checkWinner() {
    // Získání hodnoty ruky krupiéra
    const dealerValue = this.getHandValue(this.dealerHand);
    // Pro každého hráče se kontroluje výsledek
    this.players.forEach((player, index) => {
      const playerValue = this.getHandValue(player.hand);
      // Pokud hráč překročí 21, je označen jako "busted"
      if (playerValue > 21) {
        this.result.innerText += `Hráč ${index + 1} prohrál je přes! `;
      } else if (dealerValue > 21 || playerValue > dealerValue) {
        // Hráč vyhrává, pokud krupiér překročí 21 nebo má větší hodnotu než krupiér
        this.result.innerText += `Hráč ${index + 1} vyhrál! `;
      } else if (dealerValue > playerValue) {
        // Krupiér vyhrává, pokud má větší hodnotu než hráč
        this.result.innerText += `Dealer vyhrává proti Hráči ${index + 1}! `;
      } else {
        // V případě remízy
        this.result.innerText += `Hráč ${index + 1} vyhrál nad Dealer! `;
      }
    });
    // Zakázání tlačítek hit a stand po skončení hry
    this.toggleButtons(false);
  }

  // Metoda pro povolení nebo zakázání tlačítek hit a stand
  toggleButtons(enabled) {
    // Tlačítka hit a stand jsou povoleny nebo zakázány podle parametru "enabled"
    this.hitBtn.disabled = !enabled;
    this.standBtn.disabled = !enabled;
    this.startBtn.disabled = enabled;
  }
}

// Vytvoření instance hry
const game = new CardGame();

